import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.metadata.Metadata;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;

public class bigTextGen {
    public static void main(String[] args) {
        String folderPath = "/Users/Deepa/Downloads/solr-7.7.0/crawl_data";
        File yahooDir = new File(folderPath);
        File[] yahooFileList = yahooDir.listFiles();
        ArrayList<String> wordList = new ArrayList<>();

        try {
            for (File each : yahooFileList) {
                BodyContentHandler handler = new BodyContentHandler(-1);
                Metadata metadata = new Metadata();
                FileInputStream inputstream = new FileInputStream(each);
                ParseContext pcontext = new ParseContext();

                HtmlParser htmlparser = new HtmlParser();
                htmlparser.parse(inputstream, handler, metadata, pcontext);
                wordList.addAll(Arrays.asList((handler.toString()).split("\\W+")));
            }
            FileWriter fWriter = new FileWriter("big.txt");
            BufferedWriter writer = new BufferedWriter(fWriter);
            for(String each: wordList) {
                writer.write(each+"\n");
            }
            writer.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}