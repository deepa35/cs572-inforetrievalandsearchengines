<?php
include 'SpellCorrector.php';
include 'simple_html_dom.php';

header('Content-Type: text/html; charset=utf-8');
ini_set ('memory_limit', '1024M');
ini_set ('memory_limit', -1);

$limit = 10;
$query = isset($_REQUEST['q']) ? $_REQUEST['q'] : false;
$pageRankParam = array('sort' => 'pageRankFile desc');
$results = false;
$spellCorrOutput = "";

if ($query) {
    require_once('solr-php-client/Apache/Solr/Service.php');
    $solr = new Apache_Solr_Service('localhost', 8983, '/solr/hw4core/');
    $option = isset($_REQUEST['optRadio']) ? $_REQUEST['optRadio'] : false;

    if (get_magic_quotes_gpc() == 1) {
        $query = stripslashes($query);
    }

    $queryArr = explode(" ", $query);
    $correctQuery = "";
    foreach ($queryArr as $each) {
        $correctEach = SpellCorrector::correct($each);
        $correctQuery = $correctQuery.trim($correctEach)." ";
    }

    $correctQuery = trim($correctQuery);

    if(strtolower($correctQuery) != strtolower($query)) {
        $req = "http://localhost:8888/?q=$correctQuery&optRadio=$option";
        $spellCorrOutput = "Show results for <a href='$req'>$correctQuery</a> instead";
    }

    try {
        if($option=="lucene") {
            $results = $solr->search($query, 0, $limit);
        } else if ($option == "pageRank") {
            $results = $solr->search($query, 0, $limit, $pageRankParam);
        }
    }
    catch (Exception $e) {
        die("<html><head><title>SEARCH EXCEPTION</title><body><pre>{$e->__toString()}</pre></body></html>");
    }
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Yahoo Search</title>
    <style>
        html {
            background-color: white;
        }

        .formHolder {
            border: 1px solid #C8C8C8;
            margin: 25px auto auto;
            background-color: #F8F8F8;
            border-radius: 10px;
            padding-top: 2%;
            max-width: 90%;
            width: 700px;
            padding-left: 5%;
            padding-right: 5%;
        }
        th,td {
            padding: 5px !important;
        }
    </style>



</head>
<body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<div class="formHolder container">
    <form class="form-horizontal" accept-charset="utf-8" method="get">
        <h3 class="page-header" style="text-align: center;"><b>Yahoo Search</b></h3>
        <div class="form-group row" style="padding-left: 2%;padding-right:2%; padding-top: 3%;">
            <label for="query" class="control-label col-sm-3 labels">Search</label>
            <div class="col-sm-9" id="query">
                <input type="text" class="form-control" list="autoComSugg" name="q" id="q" placeholder="Enter the search query" value="<?php echo htmlspecialchars($query, ENT_QUOTES, 'utf-8'); ?>" required/>
                <datalist id="autoComSugg"></datalist>
            </div>
        </div>
        <div class="form-group row" style="padding-left: 2%;padding-right:2%;">
            <label  class="control-label col-sm-3 labels">Option</label>
            <div class="form-check form-check-inline" style="padding-left: 3%;">
                <input class="form-check-input" type="radio" name="optRadio" id="lucene" value="lucene" <?php if(!isset($_REQUEST['optRadio'])||$_REQUEST['optRadio']=="lucene"){ echo "checked";} ?>>
                <label class="form-check-label" for="lucene">Lucene</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="optRadio" id="pageRank" value="pageRank" <?php if($_REQUEST['optRadio']=="pageRank"){ echo "checked";} ?>>
                <label class="form-check-label" for="pageRank">Page Rank</label>
            </div>
        </div>
        <div class="form-group row" style="padding-left: 27.5%;padding-right:2%; padding-bottom: 3%">
            <div class="col-sm-9"></div>
            <div class="col-sm-9">
                <input type="submit" class="btn btn-primary justify-content-center align-content-between" style="margin-right: 10px">
            </div>
        </div>
    </form>
</div>
<div class="container" style="padding-top: 2%">
    <?php
    if(strlen($spellCorrOutput) > 0) {
        ?>
        <p><?php echo $spellCorrOutput; ?></p>
        <?php
    }
    ?>
</div>
<?php
if ($results) {
$total = (int) $results->response->numFound;
$start = min(1, $total);
$end = min($limit, $total);
?>
<div class="container" <?php if(strlen($spellCorrOutput) <= 0) { ?>style="padding-top: 2%" <?php } ?>>
    <?php if(strlen($spellCorrOutput) > 0) {
    ?>
    <p style="margin-top: 5%"><?php $spellCorrOutput?></p>
    <?php
    }
    ?>
    <?php
    foreach ($results->response->docs as $doc) {
        ?>
        <table class="table table-striped table-bordered" style="text-align: left">
            <?php
            $title = "";
            $url = "";
            $id = "";
            $snippet = "N/A";

            if($doc->title == false || $doc->title == null) {
                $title = "N/A";
            } else {
                $title = is_string($doc->title) ? $doc->title : $doc->title[0];
            }

            if($doc->id == false || $doc->id == null) {
                $id = "N/A";
            } else {
                $id = $doc->id;
            }

            if($doc->og_url == false || $doc->og_url == null) {
                $csv_file = fopen("URLtoHTML_yahoo_news.csv","r");
                while (!feof($csv_file)) {
                    $line = fgetcsv($csv_file);
                    $fileNameInCSV = $line[0];
                    $urlInCSV = $line[1];
                    $fullFileName = "/Users/Deepa/Downloads/solr-7.7.0/crawl_data/".$fileNameInCSV;
                    if($fullFileName == $id) {
                        $url = $urlInCSV;
                        break;
                    }
                }
                fclose($csv_file);
            } else {
                $url = $doc->og_url;
            }

            $fileContent = (file_get_html($id))->find('p');
            $queryTerms = explode(" ", $query);
            $numTerms = count($queryTerms);
            $numFound = 0;
            $lowestIndex = PHP_INT_MAX;

            foreach ($fileContent as $line) {
                $line = strip_tags($line);
                $numFound = 0;

                if(strpos(strtolower($line),strtolower($query)) !== false) {
                    $snippet = $line;
                    $numFound= $queryTerms;
                    $lowestIndex = strpos(strtolower($line),strtolower($query));
                    break;
                }

                foreach($queryTerms as $eachTerm) {
                    if(strpos(strtolower($line),strtolower($eachTerm)) !== false) {
                        $numFound = $numFound + 1;
                    }
                }

                if($numFound==$numTerms) {
                    $snippet=$line;
                    break;
                }
            }

            if(strlen($snippet)>160) {
                $before="";
                $remaining="";
                $endLength = 0;
                foreach ($queryTerms as $eachTerm) {
                    if(strpos(strtolower($snippet),strtolower($eachTerm)) < $lowestIndex) {
                        $lowestIndex = strpos(strtolower($snippet),strtolower($eachTerm));
                    }
                }

                if($lowestIndex>50) {
                    $before ="...".substr($snippet,$lowestIndex-50,50);
                    $endLength = 110;
                } else {
                    $before=substr($snippet,0,$lowestIndex);
                    $endLength = 160-$lowestIndex;
                }

                $remaining = substr($snippet,$lowestIndex);

                if(strlen($remaining)>$endLength) {
                    $remaining = substr($snippet,$lowestIndex,$endLength)."...";
                }
                $snippet = $before.$remaining;
            }

            foreach($queryTerms as $eachTerm) {
                $snippet = str_ireplace($eachTerm,"<strong>".$eachTerm."</strong>",$snippet);
            }

            ?>
            <tbody>
            <tr>
                <th scope="row"><?php echo htmlspecialchars("Title", ENT_NOQUOTES, 'utf-8'); ?></th>
                <td><a target="_blank" href="<?php echo htmlspecialchars($url, ENT_NOQUOTES, 'utf-8'); ?>">
                        <?php echo htmlspecialchars($title, ENT_NOQUOTES, 'utf-8'); ?>
                    </a></td>
            </tr>
            <tr>
                <th scope="row"><?php echo htmlspecialchars("URL", ENT_NOQUOTES, 'utf-8'); ?></th>
                <td><a target="_blank" href="<?php echo htmlspecialchars($url, ENT_NOQUOTES, 'utf-8'); ?>">
                        <?php echo htmlspecialchars($url, ENT_NOQUOTES, 'utf-8'); ?>
                    </a></td>
            </tr>
            <tr>
                <th scope="row"><?php echo htmlspecialchars("ID", ENT_NOQUOTES, 'utf-8'); ?></th>
                <td><?php echo htmlspecialchars($id, ENT_NOQUOTES, 'utf-8'); ?></td>
            </tr>
            <tr>
                <th scope="row"><?php echo htmlspecialchars("Snippet", ENT_NOQUOTES, 'utf-8'); ?></th>
                <td><?php echo ($snippet); ?></td>
            </tr>
            </tbody>
        </table>

        <?php
    }
    ?>
    <?php
    }
    ?>
</div>
<script>
    $(function() {
       $('#q').autocomplete({
            minLength: 1,
            source:function(req, res) {
                var autoComplete = [];
                var query = $("#q").val().toLowerCase();
                var lastSpaceIndex = query.lastIndexOf(" ");
                var queryBefore = "";
                var querySuggPart = "";

                if(lastSpaceIndex == -1) {
                    querySuggPart=query;
                } else {
                    querySuggPart=query.substr(lastSpaceIndex+1);
                    queryBefore=query.substr(0,lastSpaceIndex);
                }
                var url="http://localhost:8983/solr/hw4core/suggest?wt=json&q="+querySuggPart;
                $.ajax({
                    url: url,
                    success: function (data) {
                        var suggestions = "";
                        if(data.suggest && data.suggest.suggest) {
                            var jsonSuggest = data.suggest.suggest[querySuggPart];
                            if(jsonSuggest) {
                                suggestions = jsonSuggest.suggestions;
                            }
                        }

                        if(suggestions != "") {
                            for(var i=0;i<suggestions.length;i++) {
                                var each = suggestions[i].term;
                                if(queryBefore == "") {
                                    autoComplete.push(each);
                                } else {
                                    autoComplete.push(queryBefore+" "+each);
                                }
                            }
                            res(autoComplete);
                        }
                    },
                    dataType: 'jsonp',
                    jsonp : 'json.wrf'
                });
            }
       });
    });
</script>
</body>
</html>
