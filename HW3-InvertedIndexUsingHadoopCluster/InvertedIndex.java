import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class InvertedIndex {
	public static class InvertedIndexMapper extends Mapper<LongWritable, Text, Text, Text> {
		private Text word = new Text();
		private Text documentID = new Text();
		
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String line = value.toString();
			StringTokenizer tokenList = new StringTokenizer(line);
			documentID.set(tokenList.nextToken());
			line = line.replaceAll("[^a-zA-Z]+"," ").toLowerCase();
			tokenList = new StringTokenizer(line);
			word = new Text();
			while(tokenList.hasMoreTokens()) {
				word.set(tokenList.nextToken());
				context.write(word, documentID);
			}
		}
	}
	
	public static class InvertedIndexReducer extends Reducer<Text, Text, Text, Text> {
		@Override
		public void reduce(Text key, Iterable<Text> value, Context context) throws IOException, InterruptedException {
			Map<String,Integer> docFreq = new HashMap<>();
			Iterator<Text> iter = value.iterator();
			while(iter.hasNext()) {
				String docName = iter.next().toString();
				int count = 0;
				if(docFreq.containsKey(docName)) {
					count = docFreq.get(docName);
					count++;
				} else {
					count = 1;
				}
				docFreq.put(docName, count);
			}
			StringBuilder valueText = new StringBuilder("");
			for(String each: docFreq.keySet()) {
				valueText.append(each+":"+docFreq.get(each)+"\t");
			}
			context.write(key, new Text(valueText.toString()));
		}
	}
	
	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
		Configuration config = new Configuration();
		Job job = Job.getInstance(config,"Inverted Index");
		job.setJarByClass(InvertedIndex.class);
		job.setMapperClass(InvertedIndexMapper.class);
		job.setReducerClass(InvertedIndexReducer.class);
		job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    FileInputFormat.addInputPath(job, new Path(args[0]));
	    FileOutputFormat.setOutputPath(job, new Path(args[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);	
	}
}
